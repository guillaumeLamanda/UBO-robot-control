# Robot control with GPIO on raspberry pi with nodeJS

<h2> Get Starting </h2>
1. Clone the project
https://gitlab.com/guillaumeLamanda/UBO-robot-control.git

2. Install pigpio C library <br/>
`wget abyz.co.uk/rpi/pigpio/pigpio.zip
unzip pigpio.zip
cd PIGPIO
make
sudo make install`

3. Install dependancies <br/>
`npm install`

4. Launch as root user <br/>
`sudo npm start`

5. Go to the interface
Visit http://localhost:3000

<h2> Electronics assembly </h2>
![alt text](public/images/RPI_Sketch_bb.png "Electronic assembly")
